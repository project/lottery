Description
===========
Lottery API module provides an API for displaying information from lotteries 
around the world. Unless you're installing another Drupal module that 
requires it, this software is probably unnecessary.


Installation & Use
==================
Lottery API needs at least one lottery data provider module. See API.txt to
create your own.

1. Enable module in module list located at admin/build/modules.

2. Enable at least one lottery data provider (e.g. lotteryEXAMPLE) module 
   to enable settings.

3. Head to admin/settings/lottery to set what you want to show on each
   block.

4. Head to admin/build/block and put the lottery block wherever you want.


Benefits
========
If you're a Drupal developer, check out API.txt for detailed instructions
on using the Lottery API. It allows every module to provide their lottery
information with simple hooks.


lotteryEXAMPLE
============
Want to display results from any lottery that doesn't exist in a module? 
Not sure how to write a module? Worry no more, it's now quite easy.

1. Copy and rename the lotteryEXAMPLE.info and lotteryEXAMPLE.module replacing
   every instance of EXAMPLE with a descriptive, appropriate word.

2. Edit the .module file and change hooks to provide whatever lottery you want
   to show.

3. Enable the module and enjoy!

You should also want to read the API.txt.